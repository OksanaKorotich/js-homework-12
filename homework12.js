
'use strict';


let buttons = document.querySelectorAll('.btn');

document.addEventListener("keydown", (event) =>{

    buttons.forEach((item) =>{
        if(item.classList.contains('active')){
            item.classList.remove('active');
        };
    });

    let target = event.code;
    document.querySelector(`[data-code = ${target}]`).classList.add('active');

});



